// Database schema definition

// ----- //
// Usage //
// ----- //

// Exports a promise. Use like:

// db.then(function(models) {
//     console.log(models);
// });

const Sequelize = require("sequelize")
const logger = require("./logger.js")("db")

const sequelize = new Sequelize("database", "test", "test", {
  host: "localhost",
  dialect: "sqlite",

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  // SQLite only
  storage: "db.sqlite"
})

// --------- //
// DB models //
// --------- //

const Shelf = sequelize.define("shelf", {
  position: Sequelize.STRING,
  antenna_id: Sequelize.STRING,
  is_return_shelf: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  }
})
const Book = sequelize.define("book", {
  rfid: {
    type: Sequelize.STRING,
      unique: true,
      primaryKey: true
  },
  nfcid: Sequelize.STRING,
  title: Sequelize.STRING,
  author: Sequelize.STRING,
  availability: {
    type: Sequelize.ENUM,
    values: [
      "shelf", // Book is shelved (this does not comment on the location)
      "off_the_shelf", // Books is not on loan, but isn't on a shelf either
      "loan" // Book is loaned to somewhere
    ],
    defaultValue: "off_the_shelf"
  },
  shelf: {
    type: Sequelize.INTEGER,
    references: {
      model: Shelf,
      key: 'id'
    }
  }
})

const User = sequelize.define("user", {
  telegram_id: { type: Sequelize.INTEGER, primaryKey: true, unique: true },
  name: Sequelize.STRING,
  phone: Sequelize.STRING
})

const Loan = sequelize.define("loan", {
  begin: Sequelize.DATE,
  end: Sequelize.DATE,
  completed: {
    type: Sequelize.BOOLEAN,
      defaultValue: 0
  },
  user: {
    type: Sequelize.INTEGER,
    references: {
      model: User,
      key: 'telegram_id'
    }
  },
  book: {
    type: Sequelize.STRING,
    references: {
      model: Book,
      key: 'rfid'
    }
  }
});

// Book events (mainly for statistics)
const Event = sequelize.define("event", {
  time: {
    type: Sequelize.DATE,
    allowNull: false
  },
  type: {
    type: Sequelize.ENUM,
    values: [
      "unshelved", // Book was taken from the self
      "shelved", // Book was placed on a shelf
      "borrowed", // Somebody borrowed the book
      "renewed", // A user renewed the book
      "returned", // This book was returned
      "alarmed" // This book caused a door alarm
    ]
  },
  user: {
    type: Sequelize.INTEGER,
    references: {
      model: User,
      key: 'telegram_id'
    }
  },
  book: {
    type: Sequelize.STRING,
    references: {
      model: Book,
      key: 'rfid'
    }
  },
  shelf: {
    type: Sequelize.INTEGER,
    references: {
      model: Shelf,
      key: 'id'
    }
  }
})

// A shelved book has the following states:
//  1. The book is shelved in its long-term stable shelf
//  2. The book is shelved in a return shelf
//  3. The book is categorically misshelved
const BookValidPosition = sequelize.define("book_valid_position", {
  type: {
    type: Sequelize.ENUM,
    values: [
      "home", // One of the book's long-term homes
      "returned" // It's valid to return the book here
    ],
    allowNull: false
  },
  book: {
    type: Sequelize.STRING,
    references: {
      model: Book,
      key: 'rfid'
    }
  },
  shelf: {
    type: Sequelize.INTEGER,
    references: {
      model: Shelf,
      key: 'id'
    }
  }
})

// Non-shelf antenna (exit barrier or checkout scanner)
const Scanner = sequelize.define("scanner", {
    type: {
        type: Sequelize.ENUM,
        values: [
            'barrier', // Exit barrier: can't pass this without a loan
            'checkout' // Checkout scanner: helper device for borrowing books
        ]
    },
    antenna_id: {
        type: Sequelize.STRING,
        unique: true,
        primaryKey: true
    }
});

// --------------- //
// Synchronization //
// --------------- //

async function createTestData() {
  // Create some users
  var users = await Sequelize.Promise.all([
      User.create({
        telegram_id: 1,
        name: "Alice"
      }),
      User.create({
        telegram_id: 2,
        name: "Bob"
      }),
      User.create({
        telegram_id: 3,
        name: "Charlie"
      })
  ]);

  var alice = users[0];
  var bob = users[1];
  var charlie = users[2];

  // Create some books
  var books = await Sequelize.Promise.all([
      Book.create({
        rfid: "E28068100000003C4E1F9521",
        nfcid: "1",
        title: "Snow Crash",
        author: "Duffy Duck"
      }),
      Book.create({
        rfid: "E28068100000003C4E1F401D",
        nfcid: "2",
        title: "The Amateur Cracksman",
        author: "Mickey Mouse"
      }),
      Book.create({
        rfid: "E28068100000003C4E1FD805",
        nfcid: "3",
        title: "Javascript: the definitive guide",
        author: "Jim Carry"
      }),
      Book.create({
        rfid: "E28068100000003C4E1FD744",
        nfcid: "4",
        title: "Algorithms",
        author: "My Cousin"
      }),
      Book.create({
        rfid: "E28068100000003C4E1FD77E",
        nfcid: "5",
        title: "The Cathedral and the Bazaar",
        author: "The other cousin of mine"
      }),
      Book.create({
        rfid: "E28068100000003C4E1ED4E7",
        nfcid: "7",
        title: "Men At Arms",
        author: "Terry Pratchett"
      }),
      
    ]);

  // Create some shelves
  var shelves = await Sequelize.Promise.all([
      Shelf.create({
        position: "upper",
        antenna_id: "1"
      }),
      Shelf.create({
        position: "medium",
        antenna_id: "2"
      }),
      Shelf.create({
        position: "lower",
        antenna_id: "3"
      })
    ]);

  // Assign "home" shelves to the books
  //  Books 1-2 go on shelf 1
  //  Books 3-4 go on shelf 2
  //  Book 5 goes anywhere
  var validPositions = await Sequelize.Promise.all([
      BookValidPosition.create({
        type: "home",
        book: books[0].rfid,
        shelf: shelves[0].id
      }),
      BookValidPosition.create({
        type: "returned",
        book: books[0].rfid,
        shelf: shelves[2].id
      }),
      BookValidPosition.create({
        type: "home",
        book: books[1].rfid,
        shelf: shelves[0].id
      }),
      BookValidPosition.create({
        type: "returned",
        book: books[1].rfid,
        shelf: shelves[2].id
      }),
      BookValidPosition.create({
        type: "home",
        book: books[2].rfid,
        shelf: shelves[1].id
      }),
      BookValidPosition.create({
        type: "returned",
        book: books[2].rfid,
        shelf: shelves[2].id
      }),
      BookValidPosition.create({
        type: "home",
        book: books[3].rfid,
        shelf: shelves[1].id
      }),
      BookValidPosition.create({
        type: "returned",
        book: books[3].rfid,
        shelf: shelves[2].id
      }),
      BookValidPosition.create({
        type: "home",
        book: books[4].rfid,
        shelf: shelves[0].id
      }),
      BookValidPosition.create({
        type: "home",
        book: books[4].rfid,
        shelf: shelves[1].id
      }),
      BookValidPosition.create({
        type: "returned",
        book: books[4].rfid,
        shelf: shelves[2].id
      }),
      BookValidPosition.create({
        type: "home",
        book: books[5].rfid,
        shelf: shelves[1].id
      }),
      BookValidPosition.create({
        type: "returned",
        book: books[5].rfid,
        shelf: shelves[2].id
      })
    ]);

  // Create some history
  await Sequelize.Promise.all([
    Event.create({
      time: "2017-01-01 12:00:00Z",
      type: "borrowed",
      user: alice.telegram_id,
      book: books[0].rfid
    }),
    Event.create({
      time: "2017-01-06 14:00:00Z",
      type: "returned",
      user: alice.telegram_id,
      book: books[0].rfid
    }),
    Event.create({
      time: "2017-01-06 16:00:00Z",
      type: "borrowed",
      user: bob.telegram_id,
      book: books[0].rfid
    }),
    Event.create({
      time: "2017-01-24 22:00:00Z",
      type: "renewed",
      user: bob.telegram_id,
      book: books[0].rfid
    }),
    Event.create({
      time: "2017-02-12 17:00:00Z",
      type: "returned",
      user: bob.telegram_id,
      book: books[0].rfid
    })
  ])
}

// ------- //
// Exports //
// ------- //

var models = {
  Book: Book,
  Shelf: Shelf,
  User: User,
  Loan: Loan,
  Event: Event,
  BookValidPosition: BookValidPosition,
  Scanner: Scanner
}

module.exports = Sequelize.Promise.all([
   Book.sync({ force: true  }),
   Shelf.sync({ force: true  }),
   User.sync({ force: true  }),
   Loan.sync({ force: true  }),
   Event.sync({ force: true  }),
   BookValidPosition.sync({ force: true  }),
   Scanner.sync({ force: true  })
//   Book.sync({ force: true }),
//   Shelf.sync({ force: true }),
//   User.sync({ force: true }),
//   Loan.sync({ force: true }),
//   Event.sync({ force: true }),
//   BookValidPosition.sync({ force: true }),
//   Scanner.sync({ force: true })
])
  .then(createTestData)
  .then(function() {
    return models
  })
