const log = require("./logger.js")("doors")
const db = require("./db_helper.js")
const _ = require("lodash")
const rfidagent = require("./rfidagent.js")

//Send two events (door-lock, door_id)  (door-unlock, door_id)
const emitter = require("./eventbus.js")


const doors = {}

class Door {
  constructor(id, antenna_id) {
    this.id = id
    this.antenna_id = antenna_id
    this.current_books = []
    this.open = true
  }

    addBook (rfid) {
      this.current_books.push(rfid);
      try {
        db.getBookByRfidId(rfid).then(book => {
          if (book != null || book != undefined){
              if (book.availability != "loan"){
                  this.open = false;
                  emitter.emit("door-lock", this.id);
              }
              log.warn("Door open: ", this.open);
          }
        });
      } catch (err){
        log.error(err);
      }
    }

  removeBook(rfid) {
    _.remove(this.current_books, i => i == rfid)
    if(this.current_books.length == 0){
        this.open = true;
        emitter.emit("door-unlock", this.id);
        log.warn("Door open: ", this.open);
    }
  }
}

_.forEach(db.getDoorsAntennas(), door => {
  doors[door.antenna_id] = new Door(door.id, door.antenna_id)
})

rfidagent.events.on("delta",  async payloads => {
  _.forEach(payloads, async payload => {
    if (_.has(doors, payload.antenna_id)) {
      if (payload.type == "left") {
        await doors[payload.antenna_id].removeBook(payload.rfid)
       // log.debug("OUT DOOR: ", payload)
      }
      else {
        await doors[payload.antenna_id].addBook(payload.rfid)
       // log.debug("IN DOOR:", payload)
      }
    }
  });
})

var getBooksForDoor = function(door_id) {
  let door = _.find(doors, {"id": door_id});
  if (door != undefined) {
    return door.current_books
  } else {
    return []
  }
}

var unlockDoor = function(door_id) {
  let door = _.find(doors, {"id": door_id});
  if (door){
    door.open = true
    emitter.emit("door-unlock", door_id);
    log.warn("Door open: ", door.open);
  }
}

module.exports = {
  emitter,
  doors,
  getBooksForDoor,
  unlockDoor
}
