// Include modules (~libraries)
const Telegraf = require("telegraf")
const Scene = require("telegraf/scenes/base")
const Stage = require("telegraf/stage")
const session = require("telegraf/session")
const Markup = require("telegraf/markup")
const userInfoNameScene = require("./scenes/userInfoNameScene")
const userInfoCellScene = require("./scenes/userInfoCellScene")
const searchBookScene = require("./scenes/searchBookScene")
const searchLoanScene = require("./scenes/showLoanScene")
const log = require("../logger.js")("bot")
const api = require("../api.js");

const outdent = require("outdent")
const {
  getBookInfo,
  getBooksForCheckout,
  loanBook,
  loanBooksAtDoor,
  getUser,
  getBookStats,
  sendNotificationForReturn,
  renewLoan
} = require("../bot_interface")
require("dotenv").config()

const bot = new Telegraf(process.env.BOT_TOKEN)

const stage = new Stage(
  [userInfoNameScene, userInfoCellScene, searchBookScene, searchLoanScene],
  { ttl: 10 }
)

bot.use(session())
bot.use(stage.middleware())

bot.start(async ctx => {
    const tagId = ctx.message.text.substring(7)
    const user = await getUser(ctx.message.chat.id)
    if (tagId === "") {
      // No nfc tags detected. Enter in the user info scene
      return ctx.scene.enter("user-info-name-scene")
    } else if (tagId === process.env.NFC_CHECKOUT_ID) {
      // Checkout at the doors with all the books
      if (!user) {
        ctx.reply("You haven't signed up yet.")
        ctx.session.needsToCheckout = true;
        ctx.session.tagId = tagId;
        return ctx.scene.enter("user-info-name-scene")
      }
      const listOfBooks = await getBooksForCheckout(tagId)
      let replyMessage
      listOfBooks.length
        ? (replyMessage = listOfBooks
            .map(el => {
              return outdent`
                *Author*: ${el.author}
                *Title*: ${el.title}
                ${el.availability ? "Available ✅" : "Taken ❌"}
              `
            })
            .join("\n\n"))
        : (replyMessage = "There are no books to checkout!")
      return ctx.replyWithMarkdown(
        replyMessage,
        Markup.inlineKeyboard([
          Markup.callbackButton(
            "Check out",
            `checkout:${ctx.message.chat.id}:${tagId}`
          )
        ]).extra()
      )
    }

    if (!user) {
      ctx.reply("You haven't signed up yet.")
      ctx.session.needsToBorrowaBook = true;
      ctx.session.tagId = tagId;
      return ctx.scene.enter("user-info-name-scene")
    }
    const book = await getBookInfo(tagId)
    const replyMessage = outdent`
      *Author*: ${book.author}
      *Title*: ${book.title}
      ${book.availability !== "loan" ? "Available ✅" : "Taken ❌"}
    `
    // Print the query, set a new keyboard and print buttons
    ctx.replyWithMarkdown(
      replyMessage,
      Markup.inlineKeyboard([
        Markup.callbackButton(
          "Borrow",
          `borrow:${ctx.message.chat.id}:${tagId}`
        ),
        Markup.callbackButton("More info", `info:${tagId}`)
      ]).extra()
    )
  })
  .catch(err => console.log("error on start", err))

// actions in case the user press the inline button
bot.action(/borrow/, async (ctx, next) => {
  // unpack data
  const [action, telegramUserId, bookId] = ctx.update.callback_query.data.split(
    ":"
  )

  const r = await loanBook(telegramUserId, bookId);
  if (r.result) {
    return ctx.answerCbQuery().then(async () => {
      const book = await getBookInfo(bookId);
      return ctx.reply(
        `You have correctly borrowed: ${book.title} .`
      )
    })
  } else if (!r.borrowed){
    ctx.answerCbQuery().then(() => {
      return ctx.reply(
        `There has been an error. Try again or contact us support@shelfy.org`
      )
    })
  } else {
    ctx.answerCbQuery().then(() => {
      return ctx.reply(
        `This book is already borrowed.`
      )
    })
  }
})

bot.action(/info/, async (ctx, next) => {
  const [action, bookId] = ctx.update.callback_query.data.split(":")
  //console.log("bookId", bookId)

  const result = await getBookStats(bookId)
  log.info(result.stats)
  ctx.answerCbQuery().then(() => {
    return ctx.replyWithMarkdown(
      outdent `
       *Title*: ${result.book.title}
       *Author*: ${result.book.author}
       *Number of Loans*: ${result.stats.num_loans}
       *% time on shelf*: ${Math.round(result.stats.percent_time_shelved *100) / 100} %
       *% time borrowed*: ${Math.round(result.stats.percent_time_borrowed *100) / 100} %
       *In library reading time*: ${Math.round(result.stats.time_unshelved / 60000)} min
      `
      );
  })
})

bot.action(/checkout/, async (ctx, next) => {
  const [action, telegramUserId, doorId] = ctx.update.callback_query.data.split(
    ":"
  )
  const result = await loanBooksAtDoor(telegramUserId, doorId)
  if (result) {
    return ctx.answerCbQuery().then(() => {
      return ctx.reply(
        `Checkout completed correctly. You can now exit the door.`
      )
    })
  }
  ctx.answerCbQuery().then(() => {
    return ctx.reply(
      `There has been an error. Try again or contact us support@shelfy.org`
    )
  })
});

bot.action(/renew/, async (ctx, next) => {
  const [action, telegramUserId, loanid] = ctx.update.callback_query.data.split(
    ":"
  )
  await renewLoan(loanid);
  return ctx.reply(`Renewed!`)

});

bot.command("/edit_info", ctx => {
  ctx.scene.enter("user-info-name-scene")
})

bot.command("/search", ctx => ctx.scene.enter("search-book-scene"))

bot.command("/loans", ctx => {
  ctx.scene.enter("show-loan-scene")
})

bot.startPolling()

sendNotificationForReturn( (event, payload) => {
  bot.telegram.sendMessage(payload.userid, "Returned book: "+ payload.book.title);
})

process.on("uncaughtException", function(err) {
  // handle the error safely
  console.log(err.stack)
  process.exit(1)
})
