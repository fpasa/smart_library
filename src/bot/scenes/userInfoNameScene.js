const Scene = require("telegraf/scenes/base")
const Stage = require("telegraf/stage")
const Markup = require("telegraf/markup")
// declare a new scene called user-blablabla
const userInfoNameScene = new Scene("user-info-name-scene")

// write a message inside the scene
userInfoNameScene.enter(ctx =>
  ctx.reply(
    "Insert your first and last name",
    Markup.keyboard(["🔙 Back"])
      .resize()
      .extra()
  )
)

userInfoNameScene.hears("🔙 Back", ctx => {
  ctx.reply("Ok, getting back to the menu", Markup.removeKeyboard().extra())
  ctx.scene.leave()
})
userInfoNameScene.on("message", ctx => {
  // save first and last name
  ctx.session.name = ctx.message.text
  // enter in the cell scene
  ctx.scene.enter("user-info-cell-scene")
})

module.exports = userInfoNameScene
