const Scene = require("telegraf/scenes/base")
const Markup = require("telegraf/markup")
const queries = require("../../queries.js")
const bot_interface = require("../../bot_interface.js")
const log = require("../../logger.js")("show_loans_scene")
const outdent = require("outdent")

const showLoanScene = new Scene("show-loan-scene")
showLoanScene.enter(async ctx =>{ 
    ctx.reply(
      "Here follows all your loans.\nIf you like to renew any of them just press the button below the book.",
      Markup.keyboard(["🔙 Back"])
        .resize()
        .extra()
    )
    const user_string = ctx.message.chat.id
  
    // Query the db for the books and get the result
    const loan_results = await bot_interface.getListOfLoans(user_string)
    // const loans = loan_result.results; // SISTEMO! 
  
    if (!loan_results.length) {
        ctx.replyWithMarkdown(`You have no loans.`)
    }
    else {
      loan_results
        .map(result => {
          var expiring_date = result.end;
          ctx.replyWithMarkdown(
            outdent`
              *Author*: ${result.author}
              *Title*: ${result.title}
              *Return before*: ${expiring_date}`,
            Markup.inlineKeyboard([
              Markup.callbackButton(
                "Renew",
                `renew:${ctx.message.chat.id}:${result.loanid}`
              )
            ]).extra()
          )
      })
    }
  }
  
);

showLoanScene.hears("🔙 Back", ctx => {
  ctx.reply("Ok, getting back to the menu", Markup.removeKeyboard().extra())
  ctx.scene.leave()
})



module.exports = showLoanScene
