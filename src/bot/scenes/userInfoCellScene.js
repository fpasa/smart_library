const Scene = require("telegraf/scenes/base")
const { saveUser } = require("../../bot_interface")
const outdent = require("outdent")
const userInfoCellScene = new Scene("user-info-cell-scene")
const Markup = require("telegraf/markup")
const {
  getBookInfo,
  getBooksForCheckout,
  loanBook,
  loanBooksAtDoor,
  getUser,
  sendNotificationForReturn
} = require("../../bot_interface")

userInfoCellScene.enter(ctx => ctx.reply("Insert your cellphone number"))

userInfoCellScene.hears(
  /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i,
  async ctx => {
    // Save the user model
    await saveUser({
      telegramId: ctx.message.chat.id,
      name: ctx.session.name,
      phone: ctx.message.text
    })
    ctx.replyWithMarkdown(
      outdent`
        Your credentials have been saved!
      `,
      Markup.removeKeyboard().extra()
    )
    
    // Check if the user where going to check out
    if (ctx.session.needsToCheckout == true && ctx.session.tagId){
      const listOfBooks = await getBooksForCheckout(ctx.session.tagId)
      let replyMessage
      listOfBooks.length
        ? (replyMessage = listOfBooks
            .map(el => {
              return outdent`
                *Author*: ${el.author}
                *Title*: ${el.title}
                ${el.availability ? "Available ✅" : "Taken ❌"}
              `
            })
            .join("\n"))
        : (replyMessage = "There are no books to checkout!")
      return ctx.reply(
        replyMessage,
        Markup.inlineKeyboard([
          Markup.callbackButton(
            "Check out",
            `checkout:${ctx.message.chat.id}:${ctx.session.tagId}`
          )
        ]).extra()
      )
    } else if (ctx.session.needsToBorrowaBook == true && ctx.session.tagId){
      const book = await getBookInfo(ctx.session.tagId)
      const replyMessage = outdent`
        *Author*: ${book.author}
        *Title*: ${book.title}
        ${book.availability !== "loan" ? "Available ✅" : "Taken ❌"}
      `
      // Print the query, set a new keyboard and print buttons
      ctx.replyWithMarkdown(
        replyMessage,
        Markup.inlineKeyboard([
          Markup.callbackButton(
            "Borrow",
            `borrow:${ctx.message.chat.id}:${ctx.session.tagId}`
          ),
          Markup.callbackButton("More info", `info:${ctx.session.tagId}`)
        ]).extra()
      )  
    }

    ctx.scene.leave()
  }
)



module.exports = userInfoCellScene
