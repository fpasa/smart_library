const Scene = require("telegraf/scenes/base")
const Markup = require("telegraf/markup")
const queries = require("../../queries.js")
const log = require("../../logger.js")("search_scene")

const searchBookScene = new Scene("search-book-scene")
searchBookScene.enter(ctx =>
  ctx.reply(
    "Send me the name or the author of the book you want to find.",
    Markup.keyboard(["🔙 Back"])
      .resize()
      .extra()
  )
)

searchBookScene.hears("🔙 Back", ctx => {
  ctx.reply("Ok, getting back to the menu", Markup.removeKeyboard().extra())
  ctx.scene.leave()
})

searchBookScene.on("message", async ctx => {
  console.log("ctx", ctx)
  const search_string = ctx.message.text

  // Query the db for the books and get the result
  const search_result = await queries.searchForBook({ title: search_string })
  const results = search_result.results;

  if (!results.length) {
      ctx.replyWithMarkdown(`There are no results for *${search_string}*\nTry another query`)
  }
  else {
    const replyMessage = results
      .map(result => {
        var book = result.book;
        var shelfinfo = result.shelf ? 'shelf: ' + result.shelf.position : 'not in shelf';
        return [
          `*Author*: ${book.author}`,
          `*Title*: ${book.title}`,
          `${book.available != "loan" ? "Available ✅" : "Taken ❌"}`,
          shelfinfo
        ].join("\n")
    })
    .join("\n\n")
    ctx.replyWithMarkdown(replyMessage)
  }
})

module.exports = searchBookScene
