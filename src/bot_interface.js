const log = require("./logger.js")("doors")
const db = require("./db_helper.js")
const queries = require("./queries.js")
const doors = require("./doors.js")
const _ = require("lodash")
const rfidagent = require("./rfidagent.js")
const emitter = require("./eventbus.js")

// This function ask the list of books from the door
var getBooksForCheckout = async door_id => {
  let booksid =  await doors.getBooksForDoor(door_id);
  return Promise.all(booksid.map(id => db.getBookByRfidId(id)));;
}

var saveUser = async payload => {
  let result = await db.registerUser(payload.telegramId, payload.name, payload.phone)
  emitter.emit("new-user", payload);
  return result;
}

var getBookInfo = async nfcid => {
  return await db.getBookByNfcId(nfcid);
}

var getBookStats = async nfcid => {
  let book =  await db.getBookByNfcId(nfcid);
  log.debug(book)
  let stats = await queries.collectBookStats({book});
  return {book, stats}
}

var loanBook = async (userid, book_id) => {
    var user = await queries.getUserByTelegramId(userid);

  return await db
    .borrowBook(book_id, userid, "nfc")
    .then(async () => {
        var book = await db.getBookByNfcId(book_id);
        emitter.emit('book-borrowed', {
            book: book,
            user: user
        });
      return { result: true, borrowed: false }
    })
    .catch(error => {
      if (error == 'This book is already borrowed. You Thief!') {
        return { result: false, borrowed: true }
      }
      return { result: false, borrowed: false }
    })
}

var loanBooksAtDoor = async (userid, door_id) => {
    var user = await queries.getUserByTelegramId(userid);
  try {
    let booksids = doors.getBooksForDoor(door_id);
    if (booksids.length == 0) return "nobooks";
    for (let book_id of booksids) {
      await db.borrowBook( book_id, userid)
      var book = await db.getBookByRfidId(book_id);
      emitter.emit('book-borrowed', {
          book: book,
          user: user
      });
    }
    doors.unlockDoor(door_id)
    return true
  } catch (err) {
    log.error(err)
    return false
  }
}

var renewLoan = async (loanid) => {
  return await db.renewLoan(loanid);
}

var getListOfLoans = async (userid) => {
  return await db.getLoansByUser(userid);
}

var searchForBook = async title => {
  return await queries.searchForBook({ title: title }).results
}

var getUser = async telegramId => {
  return await queries.getUserByTelegramId(telegramId)
}

var sendNotificationForReturn = (callback)=> {
  emitter.on("book-returned", payload => callback("book-returned", payload));
}

module.exports = {
  getBooksForCheckout,
  getBookInfo,
  getBookStats,
  loanBook,
  loanBooksAtDoor,
  searchForBook,
  getUser,
  saveUser,
  sendNotificationForReturn,
  getListOfLoans,
  renewLoan
}
