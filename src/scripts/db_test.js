const db = require("../db.js");

async function createTestData() {
    let models = await db;
    
    var alice = await User.create({
      telegram_id: 1,
      name: "Alice"
    })
  
    var bob = await User.create({
      telegram_id: 2,
      name: "Bob"
    })
  
    var a = await Book.create({
      rfid: "asdasda",
      nfcid: "1",
      title: "A",
      availablility: "shelf"
    })
  
    var b = await Book.create({
      rfid: "asd345asd5354a",
      nfcid: "2",
      title: "B",
      availablility: "shelf"
    })
  
    var shelf1 = await Shelf.create({
      position: "upper",
      antenna_id: "Junction.2017.1"
    })
  
    var shelf2 = await Shelf.create({
      position: "lower",
      antenna_id: "Junction.2017.2"
    })
  
    await BookValidPosition.create({
      type: "home",
      book: a,
      shelf: shelf1
    })
    await BookValidPosition.create({
      type: "home",
      book: b,
      shelf: shelf2
    })
  
    shelf1.addBook(a)
    shelf2.addBook(b)
  
    await Sequelize.Promise.all([
      Event.create({
        time: "2017-01-01 12:00:00Z",
        type: "borrowed",
        user: alice,
        book: a,
        shelf: null
      })
    ])
  }

db.then(function(models) {
    console.log(models);
});

