const queries = require("../queries.js");

var searchTerms = ['Snow', 'at'];
for (var i in searchTerms) {
    var term = searchTerms[i];
    queries.searchForBook({
        title: term
    }).then(function(result) {
        console.log('Search of ' + result.query.title
             + ' found: ' + result.results);
    }.bind({ term: term }));
}

queries.listAllUsers().then(function(users) {
    console.log('listAllUsers() returned '
        + users.length + ' users');
});

queries.getUserByTelegramId(1).then(function(user) {
    if (user) user = user.name;
    console.log('user found by id 1: ' + user);
});

queries.getUserByTelegramId(2).then(function(user) {
    if (user) user = user.name;
    console.log('user found by id 2: ' + user);
});

queries.getUserByTelegramId(3).then(function(user) {
    if (user) user = user.name;
    console.log('user found by id 3: ' + user);
});

queries.searchForBook({ title: 'Snow Crash' }).then(result => {
    return queries.collectBookStats({
        book: result.results[0].book
    });
}).then(stats => {
    console.log(stats);
});

queries.findMisplacedBooks().then(results => {
    for (var i in results) {
        var shelf = results[i].shelf ? results[i].shelf.position : '(none)';
        console.log('Found a book with ' + results[i].type + ': ' + results[i].book.title
            + ' in shelf ' + shelf);
    }
});

queries.findBookFullInfo("E28068100000003C4E1F9521").then(info => console.log(info));
