const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const db = require('./db.js')

// Arguments:
//   querry: text that should be contained in the book title or author
// Returns Promise for:
//   {
//      query: the original query
//      results: a list of Book objects
//    }
async function searchForBook(query)
{
    var models = await db;

    var books = await models.Book.findAll({
        where: {
            [Op.or]: {
                title: {
                    [Op.like]: '%' + query.title + '%'
                },
                author: {
                    [Op.like]: '%' + query.title + '%'
                }
            }
        }
    });

    var results = [];
    for (var i in books) {
        var book = books[i];
        var result = {
            book: book,
            shelf: null
        };
        if (book.shelf) {
            var shelves = await models.Shelf.findAll({ where: { id: book.shelf } });
            if (shelves && shelves.length >= 1) {
                result.shelf = shelves[0];
            }
        }
        results.push(result);
    }

    return {
        query: query,
        results: results
    };
}

// Returns Promise for:
//  [User object, ...]
async function listAllUsers()
{
    var models = await db;
    return models.User.findAll();
}

// Returns Promise for:
//   User object, or null if not found
async function getUserByTelegramId(telegram_id)
{
    var models = await db;
    return models.User.findById(telegram_id);
}

// Collects usage statistics for a single volume.
// Arguments:
//   {
//      book: a Book object
//      start_date: starting date for the query (optional)
//      end_date: ending date for the query (optional)
//   }
// Returns promise for:
//   {
//      time_total: total time surveyed
//      num_loans: number of times this book has been loaned out
//      num_renews: number of times this book has been renewed
//      TODO: renew_probability: probability of renewing after loaning
//      TODO: mean_renew_count: mean number of times renewed
//      (percent_)time_shelved: time spent on the home shelf
//      TODO: (percent_)time_return_shelved: time spent on any return shelf
//      TODO: (percent_)time_misshelved: time spent on an incorrect shelf
//      (percent_)time_unshelved: time spent not on a shelf but not loaned
//      (percent_)time_borrowed: time spent loaned
//   }
async function collectBookStats(query)
{
    var models = await db;

    // Query to get all events with this book, in chronological order
    var q = {
        where: { book: query.book.rfid },
        order: [['time', 'ASC']]
    };

    if (query.start_date || query.end_date) {
        q.where.time = {};
        if (query.start_date) {
            q.where.time[Op.gte] = query.start_date;
        }
        if (query.end_date) {
            q.where.time[Op.lte] = query.end_date;
        }
    }

    // Calculate stats over the events
    return models.Event.findAll(q).then(events => {
        // Stats accumulated here
        var stats = {};

        // Total time spent
        var start_date = query.book.createdAt;
        if (query.start_date) {
            start_date = new Date(start_date);
        }
        else {
            for (var i in events) {
                var e = events[i];
                if (e.time < start_date) {
                    start_date = e.time;
                }
            }
        }
        var end_date = new Date();
        if (query.end_date) {
            end_date = new Date(end_date);
        }
        stats.time_total = end_date - start_date;

        // Counters
        stats.num_loans = 0;
        stats.num_renews = 0;
        stats.time_borrowed = 0;
        stats.time_shelved = 0;
        stats.time_unshelved = 0;

        var prevState = null;
        var prevStateBegin = start_date;

        for (var i in events) {
            var e = events[i];

            // Increment borrow counter
            if (e.type == 'borrowed') {
                stats.num_loans++;
            }

            // Increment renew counter
            if (e.type == 'renewed') {
                stats.num_renews++;
            }

            // Update timing
            if (e.type == 'unshelved') {
                if (!prevState || (prevState == 'shelved')) {
                    stats.time_shelved += e.time - prevStateBegin;
                }

                if (prevState != 'unshelved') {
                    prevState = 'unshelved';
                    prevStateBegin = e.time;
                }
            }
            else if (e.type == 'shelved') {
                if (!prevState || (prevState == 'unshelved')) {
                    stats.time_unshelved += e.time - prevStateBegin;
                }
                else if (prevState == 'borrowed') {
                    stats.time_borrowed += e.time - prevStateBegin;
                }

                prevState = 'shelved';
                prevStateBegin = e.time;
            }
            else if (e.type == 'borrowed') {
                if (!prevState || (prevState == 'unshelved')) {
                    stats.time_unshelved += e.time - prevStateBegin;
                }
                else {
                    stats.time_shelved += e.time - prevStateBegin;
                }

                prevState = 'borrowed';
                prevStateBegin = e.time;
            }
            else if (e.type == 'returned') {
                stats.time_borrowed += e.time - prevStateBegin;

                prevState = 'shelved';
                prevStateBegin = e.time;
            }
        }

        if (prevState == 'shelved') {
            stats.time_shelved += end_date - prevStateBegin;
        }
        else if (prevState == 'unshelved') {
            stats.time_unshelved += end_date - prevStateBegin;
        }
        else if (prevState == 'borrowed') {
            stats.time_borrowed += end_date - prevStateBegin;
        }

        stats.percent_time_shelved = 100.0 * stats.time_shelved / stats.time_total;
        stats.percent_time_unshelved = 100.0 * stats.time_unshelved / stats.time_total;
        stats.percent_time_borrowed = 100.0 * stats.time_borrowed / stats.time_total;

        return stats;
    });
}

// Finds all books that are not "home".
//
// Arguments:
//   return_only_types: list of types to include
//
// Returns Promise for:
//    [
//      {
//        type: "no_shelf" or "return_shelf" or "bad_shelf",
//        book: the Book object,
//        shelf: current Shelf object (or null),
//        returnShelves: [Shelf, ...]
//        homeShelves: [Shelf, ...]
//      },
//    ...
//    ]
async function findMisplacedBooks(return_only_types)
{
    var models = await db;

    if (!return_only_types) {
        return_only_types = ["no_shelf", "return_shelf", "bad_shelf"];
    }

    // Crud, should actually use some sql
    var books = await models.Book.findAll({});
    var shelves = await models.Shelf.findAll({});
    var valids = await models.BookValidPosition.findAll({});

    function shelfWithId(id) {
        for (var k in shelves) {
            if (shelves[k].id == id) {
                return shelves[k];
            }
        }
    }

    var results = [];
    for (var i in books) {
        var book = books[i];
        var homes = [];
        var returns = [];
        for (var j in valids) {
            if ((valids[j].book == book.rfid) && (valids[j].type == 'home')) {
                homes.push(shelfWithId(valids[j].shelf));
            }
            if ((valids[j].book == book.rfid) && (valids[j].type == 'returned')) {
                returns.push(shelfWithId(valids[j].shelf));
            }
        }

        type = null;
        shelf = null;
        if (!book.shelf) {
            type = "no_shelf";
        }
        else if (!homes.includes(book.shelf)) {
            if (returns.includes(book.shelf)) {
                type = "return_shelf";
            }
            else {
                type = "bad_shelf";
            }
            shelf = shelfWithId(book.shelf);
        }

        if (type) {
            results.push({
                type: type,
                book: book,
                shelf: shelf,
                returnShelves: returns,
                homeShelves: homes
            });
        }
    }

    return results.filter(result => return_only_types.includes(result.type));
}

// Returns book full info
async function findBookFullInfo(rfid)
{
    var models = await db;

    var book = await models.Book.findById(rfid);
    var shelf = null;
    if (book.shelf) {
        shelf = await models.Shelf.findById(book.shelf);
    }
    var homeShelfPositions = await models.BookValidPosition.findAll({
        where: {
            book: rfid,
            type: "home"
        }
    });
    var returnShelfPositions = await models.BookValidPosition.findAll({
        where: {
            book: rfid,
            type: "returned"
        }
    });
    var homeShelves = await Sequelize.Promise.all(
            homeShelfPositions.map(pos => models.Shelf.findById(pos.shelf)));
    var returnShelves = await Sequelize.Promise.all(
            returnShelfPositions.map(pos => models.Shelf.findById(pos.shelf)));

    var shelving;
    if (!shelf) {
        shelving = 'no_shelf';
    }
    else if (homeShelfPositions.map(pos => pos.shelf).includes(book.shelf)) {
        shelving = 'home_shelf';
    }
    else if (returnShelfPositions.map(pos => pos.shelf).includes(book.shelf)) {
        shelving = 'return_shelf';
    }
    else {
        shelving = 'bad_shelf';
    }

    return {
        book: book,
        shelf: shelf,
        shelf_kind: shelving,
        homeShelves: homeShelves,
        returnShelves: returnShelves
    };
}

// ------- //
// Exports //
// ------- //

module.exports = {
    searchForBook: searchForBook,
    listAllUsers: listAllUsers,
    getUserByTelegramId: getUserByTelegramId,
    collectBookStats: collectBookStats,
    findMisplacedBooks: findMisplacedBooks,
    findBookFullInfo: findBookFullInfo
};

