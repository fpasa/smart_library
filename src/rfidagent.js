var socket = require('socket.io-client')('http://balabanovo.westeurope.cloudapp.azure.com');
const log = require('./logger.js')('rfid');
const EventEmitter = require("events");
const _ = require("lodash");
const emitter = new EventEmitter();

onshelf_books = []
antennas = { 
    '1': [],
    '2': [],
    '3': [],
    '4': []
}

// notify that connection was established.
socket.on('connect', function(){
    log.info('Connected');
});

// subscribe to the raw reader data.
socket.on('inventory', function(data){
    // each reader emits itw own inventory cycle
    // the reader at Junction 2017 is having mac address 00:16:25:12:16:4F 
    // we need to filter out all other messages
    // data format is {macAddress, inventoryRecords}
    if(data.macAddress !== '00:16:25:12:16:4F'){
        return;
    }
    
    let deltas = []
    let new_antennas= {
        '1': [],
        '2': [],
        '3': [],
        '4' : []
    }
    let epcs = {}

    _.forEach(data.orderedRecords, record => {
        // Check if I'm adding a rfid but it's already there 
        if (record.antenna_port != 0) { 
           new_antennas[record.antenna_port].push(record.epc);
           if(!_.has(epcs, record.epc)){
               epcs [record.epc] = [record.antenna_port];
           }else {
            epcs [record.epc].push(record.antenna_port);
           }
        }
    });
    _.forOwn(epcs, (ports, epc)=> {
        let badport = null;
        if(ports.length > 1){
            for (port of ports){
                if(!_.indexOf(antennas[port], epc)){
                    badport = port
                }
            }
        }
        //Remove the port that's new
        if (badport != null){
            _.remove(new_antennas[badport], obj => obj==epc)
            log.info("Removed: ", port, epc);
        }
        
    })

      
    for (let antenna_id in antennas){
        let diffout = _.difference(antennas[antenna_id], new_antennas[antenna_id]);
        for (let epc of diffout){
            deltas.push({type:"left", antenna_id: antenna_id, rfid: epc});
        }
        let diffin = _.difference(new_antennas[antenna_id],antennas[antenna_id]);
        for (let epc of diffin){
            deltas.push({type:"enter", antenna_id: antenna_id, rfid: epc});
        }
    }
    deltas = _.orderBy(deltas, ['type'], ['desc'])
    antennas = new_antennas;
    emitter.emit("delta", deltas);
    //log.debug(antennas)
   
});

// what to do on disconnect
socket.on('disconnect', function(){
    log.info('Disconnected');
});

module.exports = {
    events: emitter, 
    antennas
}
