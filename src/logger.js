var winston = require('winston');

// Default configs for a logger.
// They can be overwritten by configuration
var defaults = function(name){
    // Merging defauls with defaults from configs.
    return {
        console: {
          level: 'debug',
          colorize: true,
          label: name,
          timestamp : true,
          stringify: true,
          prettyPrint: true
        },
    }
};


// The modules can ask for a logger. If the logger has not been configured
// by the user, It will be created with the default configuration.
module.exports = function(name) {
    winston.loggers.add(name, defaults(name));
    return winston.loggers.get(name);
}
