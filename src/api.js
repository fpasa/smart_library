const events = require("./eventbus.js");
const rfidagent = require("./rfidagent.js").antennas;
const db = require("./db_helper.js")
const _ = require("lodash");
const log = require("./logger.js")("api")
const queries = require("./queries.js");
var app = require('http').createServer()
var io = require('socket.io')(app);

app.listen(5000);

io.on('connection', function (socket) {
    log.debug('connected')
  events.on("book-shelfed" , async payload => {
    let book = await db.getBookByRfidId(payload.rfid);
    socket.emit("book-shelfed", book)
  });
  events.on("book-unshelfed" , async payload => {
    let book = await db.getBookByRfidId(payload.rfid);
    socket.emit("book-unshelfed", book)
  });
  events.on("book-return-shelfed" , async payload => socket.emit("book-return-shelfed", payload) );
  events.on("book-bad-shelfed" , async payload => socket.emit("book-bad-shelfed", payload ));
  events.on("door-lock" ,  door_id => socket.emit("door-lock", door_id));
  events.on("door-unlock" , door_id => socket.emit("door-unlock", door_id));
  events.on("new-user", payload => socket.emit("new-user", payload) );
  events.on("book-borrowed" , payload => socket.emit("book-borrowed", payload));
  events.on("book-returned-frontend", payload => socket.emit("book-returned", payload) );

  socket.on("get-stats", async (query) => {
    let stats = await queries.collectBookStats(query);
    socket.emit(stats);
    });
});
