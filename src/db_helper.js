const db = require("./db.js")
const Sequelize = require("sequelize")
const Op = Sequelize.Op
const log = require("./logger.js")("dbHelper")
const env = require("dotenv").config()
const rfidagent = require("./rfidagent.js").events;
const emitter = require("./eventbus.js");
const queries = require("./queries.js");


process.on("unhandledRejection", function(reason, p) {
 console.log(
   "Possibly Unhandled Rejection at: Promise ",
   p,
   " reason: ",
   reason
 )
 // application specific logging here
})

 var getBookByNfcId = async function(nfcId) {
  var models = await db

  var book = await models.Book.findOne({
    where: { nfcid: nfcId }
  })

  if (!book) {
    throw "Book does not exists."
    }

  return book
};

var getBookByRfidId = async function(rfidId) {
  var models = await db

  var book = await models.Book.findOne({
    where: { rfid: rfidId }
  })

  if (!book) {
    throw "Book does not exists (rfid: " + rfidId +  ")."
    }

  return book
};

var getShelfFromAntenna = async function (antennaId) {
  var models = await db;
  return models.Shelf.findOne({ where: { antenna_id: antennaId } })
};

// A book has been taken from the shelf -- make the changes in the database
var unshelf = async function(b, s) {
  var models = await db;

  // Push changes to the database (yield when done)
  //  - create the required event
  //  - mark the book as off the shelf, with no current shelf
  await Sequelize.Promise.all([
      models.Event.create({
          type: "unshelved",
          time: new Date(),
          book: b.rfid,
          shelf: s.id
      }),
      b.update({
        availability: "off_the_shelf",
        shelf: null
      })
    ]);
}

// A book has been placed in the shelf -- make the changes in the database
var shelf = async function(b, s) {
  var models = await db;

  // If the book is on loan, shelving it automatically returns it
  if (b.availability === "loan") {
    await returnBook(b);
  }

  // Push changes to the database (yield when done)
  //  - create the required event
  //  - mark the book as off the shelf with, no current shelf
  await Sequelize.Promise.all([
      models.Event.create({
          type: "shelved",
          time: new Date(),
          book: b.rfid,
          shelf: s.id
      }),
      b.update({
        availability: "shelf",
        shelf: s.id
      })
    ]);
}

var borrowBook = async function(bookId, userId) {
  var models = await db
  var book = await models.Book.findOne({
    where: {
      [Op.or]: {
        rfid: bookId,
        nfcid: bookId
      }
    }
  })

  if (!book) {
    throw "Book does not exists!"
    return
  }

  var user = await models.User.findOne({
    where: {
      telegram_id: userId
    }
  })

  if (!user) {
    throw "User does not exists!"
    return
  }

  if (book.availability === "loan") {
    throw "This book is already borrowed. You Thief!"
    return
  }

  let d = new Date();
  let de = new Date();
  de.setMonth(de.getMonth() +1);
  await models.Loan.create({
      begin: d,
      end: de,
      completed: 0,
      book: book.rfid,
      user: user.telegram_id
    })

  // Update book status
  await book.update({
    availability: "loan"
  })

  await models.Event.create({
    type: "borrowed",
    time: new Date(),
    book: book.rfid,
    user: user.telegram_id
  })
}

var registerUser = async function(telegramId, name, phone) {
  var models = await db

  return await models.User.findOrCreate({
    where: { telegram_id: telegramId },
    defaults: {
      telegram_id: telegramId,
      name: name,
      phone: phone
    }
  })
}

var returnBook = async function(bookOrRfid, shelfOrId /* optional */) {
  var models = await db


  var book = null
  if (typeof bookOrRfid === "string") {
    book = await models.Book.findById(bookOrRfid)
  } else {
    book = bookOrRfid
  }
  var shelf = null
  if (shelfOrId) {
    if (typeof shelfOrId === "string") {
      shelf = await models.Shelf.findById(shelfOrId)
    } else {
      shelf = shelfOrId
    }
  }

  // Find out loan
  var loan = await models.Loan.findOne({ where: { book: book.rfid } })

  // complete the load of this book
  await loan.update({
    end: new Date(),
    completed: 1
  })


  // Log event
  var event = await models.Event.create({
    type: "returned",
    time: new Date(),
    book: book.rfid,
    user: loan.user
  })

  if (shelf) {
    await event.update({ shelf: shelf.id })

    await book.update({
      availability: "shelf",
      shelf: shelf.id
    })
  } else {
    await book.update({
      availability: "off_the_shelf",
      shelf: null
    })
  }

  log.info("RETURNED BOOK: ", book.nfcId);

  var user = await models.User.findOne({
      where: { telegram_id: loan.user }
  });

  emitter.emit("book-returned", {userid: loan.user, book});
  emitter.emit("book-returned-frontend", {user: user, book: book});
}

var getLoansByUser = async (userid) => {
  let models = await db;
  let loans = await models.Loan.findAll({
    where: {
      user: userid,
      completed : 0
    }
  });
  let result = await Promise.all(loans.map(async loan => {
    let book = await getBookByRfidId(loan.book);
    return {begin: loan.begin, end: loan.end, title: book.title, author: book.author, loanid: loan.id};
  }));
  return result;
}

var renewLoan = async (loanid) => {
  let models = await db;
  let loan = await models.Loan.findById(loanid);
  let t = new Date(loan.end)
  t.setMonth(t.getMonth() +1)
  await loan.update( {
    end: t
  });
  emitter.emit("book-renewed", {userid: loan.user, book: loan.book, loanid: loan.id});
}

var getDoorsAntennas = function() {
  return [{ id: process.env.NFC_CHECKOUT_ID, antenna_id: process.env.NFC_CHECKOUT_ANTENNA_ID }]
}

// shelf / unshelf events
rfidagent.on("delta", async payloads => {
    for (var i in payloads) {
        var payload = payloads[i];
        if (payload.antenna_id != process.env.NFC_CHECKOUT_ANTENNA_ID) {
            try {
              var b = await getBookByRfidId(payload.rfid);
              if (!b) {
                  return;
              }
            } catch (err){
              log.error(err);
              return;
            }
            var s = await getShelfFromAntenna(payload.antenna_id);
            if (!s) {
                throw "Antenna id not found: " + payload.antenna_id;
            }

            if (payload.type == "left") {
                await unshelf(b, s);
                log.info("Unshelved a book: " + b.title + " (" + payload.rfid + ")");
                emitter.emit("book-unshelfed", payload);
            }
            else {
                await shelf(b, s);
                log.info("Shelved a book: " + b.title + " (" + payload.rfid + ")");
                emitter.emit("book-shelfed", payload);
            }
        }
    }
});

emitter.on("book-shelfed", async payload => {
  var fullInfo = await queries.findBookFullInfo(payload.rfid);
  if (fullInfo.shelf_kind == 'return_shelf') {
    log.info("Book placed in return shelf: " + fullInfo.book.title + " (" + payload.rfid + ")");
    emitter.emit("book-return-shelfed", fullInfo);
  }
  else if (fullInfo.shelf_kind == 'bad_shelf') {
    log.info("Book placed in incorrect shelf: " + fullInfo.book.title + " (" + payload.rfid + ")");
    emitter.emit("book-bad-shelfed", fullInfo);
  }
});





module.exports = {
  getBookByNfcId,
  getBookByRfidId,
  registerUser,
  unshelf,
  shelf,
  returnBook,
  borrowBook,
  getDoorsAntennas,
  getLoansByUser,
  renewLoan
}
