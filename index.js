//const db = require("./src/db_helper.js");
const doors = require("./src/doors.js");
const log = require("./src/logger.js")("index");

const rfidagent = require("./src/rfidagent.js");
const dbh = require('./src/db_helper.js');

//rfidagent.events.on("tagEntered", payload => log.info("ENTER: " ,payload));
//rfidagent.events.on("tagLeft", payload => log.info("LEFT: ",payload));
const api = require("./src/api.js");

process.on("unhandledRejection", function(reason, p) {
    log.error(
      "Possibly Unhandled Rejection at: Promise ",
      p,
      " reason: ",
      reason
    )
  })
